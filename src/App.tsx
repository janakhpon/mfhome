import React from "react";
import ReactDOM from "react-dom";

import "./index.scss";
const Layout = React.lazy(() => import("./Layout"));
const Navbar = React.lazy(() => import("./Navbar"));

const App = () => (
  <>
    <React.Suspense fallback={<p className="text-green-300">loading navbar</p>}>
      <Navbar />
    </React.Suspense>
    <React.Suspense
      fallback={<p className="text-yellow-300">loading layout</p>}
    >
      <Layout>
        <div className="max-w-6xl mx-auto mt-10 text-3xl">
          <div>Name: home</div>
          <div>Framework: react</div>
          <div>Language: TypeScript</div>
          <div>CSS: Tailwind</div>
        </div>
      </Layout>
    </React.Suspense>
  </>
);
ReactDOM.render(<App />, document.getElementById("app"));
