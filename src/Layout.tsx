//@ts-nocheck
import React, { ReactNode } from "react";

export default function Layout({ children }: { children: ReactNode }) {
  return (
    <div className="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
      {children}
    </div>
  );
}
